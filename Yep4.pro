#-------------------------------------------------
#
# Project created by QtCreator 2015-11-29T19:47:39
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Yep4
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    obj_metrics.cpp

HEADERS  += mainwindow.h \
    obj_metrics.h

FORMS    += mainwindow.ui

INCLUDEPATH += C:\\BigSchool\\4th\\YaP\\Yep4

QMAKE_LFLAGS += /INCREMENTAL:NO
