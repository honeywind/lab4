#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <obj_metrics.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    Obj_metrics m;
    ui->textBrowser->append("Code Lines Average");
    ui->textBrowser->append(QString::number(  m.CodeLinesAverage()));

    ui->textBrowser->append("Methods Number");
    ui->textBrowser->append(QString::number(  m.MethodsNumber()));

    ui->textBrowser->append("Moduls Number");
    ui->textBrowser->append(QString::number(  m.ModulsNumber()));

    ui->textBrowser->append("Class Number");
    ui->textBrowser->append(QString::number( m.ClassNumber()));

    ui->textBrowser->append("Class Lines Average");
    ui->textBrowser->append(QString::number( m.ClassLinesAverage()));

    ui->textBrowser->append("NORM");
    ui->textBrowser->append(QString::number( m.NORM()));

    ui->textBrowser->append("RFC");
    ui->textBrowser->append(QString::number( m.RFC()));

    ui->textBrowser->append("WMC");
    ui->textBrowser->append(QString::number( m.WMC()));

    ui->textBrowser->append("NOC");
    ui->textBrowser->append(QString::number( m.NOC()));
}

MainWindow::~MainWindow()
{
    delete ui;
}
