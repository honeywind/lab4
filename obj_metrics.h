#ifndef OBJ_METRICS_H
#define OBJ_METRICS_H

#include <QObject>
#include <QFile>
#include <math.h>
#include <QTextStream>
#include <QTextCodec>
#include <QStringList>
#include <QDebug>


class Obj_metrics : public QObject
{
    Q_OBJECT

private:
    QFile *codefile;
public:
    explicit Obj_metrics(QObject *parent = 0);
    float CodeLinesAverage();
    float MethodsNumber();
    float ModulsNumber();
    float ClassNumber();
    float ClassLinesAverage();
    int RFC(); //відгук на клас - кількість методів, які можуть викликатися примірниками класу, обчислюється як сума локальних і віддалених методів;
    float WMC(); //	зважена насиченість класу
    float NOC(); //кількість нащадків (безпосередніх)
    int NORM(); //кількість викликів віддалених методів
signals:

public slots:
};


#endif // OBJ_METRICS_H
