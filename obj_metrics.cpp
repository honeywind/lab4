#include "obj_metrics.h"


Obj_metrics::Obj_metrics(QObject *parent) : QObject(parent)
{
    codefile = new QFile("C:\\BigSchool\\4th\\YaP\\quality.cpp");
}



float Obj_metrics::MethodsNumber()
{
    if (codefile->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(codefile);
        while (!in.atEnd())
        {
            QString code = in.readAll();
            QStringList list=code.split(QRegExp("(void|int|string|float|double|byte)[A-Za-z0-9]*"),QString::SkipEmptyParts);
                float sl=list.count()-1;
                codefile->close();
                return sl;
        }
    }
}

float Obj_metrics::ModulsNumber()
{
    if (codefile->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(codefile);
        while (!in.atEnd())
        {
            QString code = in.readAll();
            QStringList lst=code.split(QRegExp("#include"),QString::SkipEmptyParts);
            float sl=lst.count()-1;
            codefile->close();
            return sl;
        }
    }
}

float Obj_metrics::ClassNumber()
{
    if (codefile->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(codefile);
        while (!in.atEnd())
        {
            QString code = in.readAll();
            QStringList lst=code.split(QRegExp("class"),QString::SkipEmptyParts);
            float sl=lst.count()-1;
            codefile->close();
            return sl;

        }
    }
}

float Obj_metrics::ClassLinesAverage() //середня кількість рядків для класів
{
    if (codefile->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(codefile);
        while (!in.atEnd())
        {
            QString code = in.readAll();
            QStringList lst=code.split(QRegExp("class"),QString::SkipEmptyParts);
            int st=0;
            for(int i=lst.count()-1;i>=0;--i)
            {
                const QString&item=lst[i];
                st= st+item.count("\n")-1;
            }
            float sl=(float)st/(lst.count());
            codefile->close();
            return sl;
        }
    }
}

float Obj_metrics::CodeLinesAverage()
{
    if (codefile->open(QIODevice::ReadOnly| QIODevice::Text))
        {
            QTextStream in(codefile);
            while (!in.atEnd())
            {
                QString code = in.readAll();
                QStringList list=code.split(QRegExp("(void|int|string|float|double|byte)[A-Za-z0-9]*"),QString::SkipEmptyParts);
                    int st=0;
                    for(int i=list.count()-1;i>=0;--i)
                    {
                        const QString& item=list[i];
                        st= st+item.count("\n");
                    }
                    float result=(float)st/(list.count())-1;
                    codefile->close();
                    return result;
            }
    }
}

float Obj_metrics::WMC() //	зважена насиченість класу
{
    if (codefile->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(codefile);
        while (!in.atEnd())
        {
            QString code = in.readAll();
            QStringList lst=code.split(QRegExp("class"),QString::SkipEmptyParts);
            int st=0;
            for(int i=lst.count()-1;i>=0;--i)
            {
                const QString&item=lst[i];
                QStringList list=item.split("){",QString::SkipEmptyParts);
                st=0;
                for(int j=list.count()-1;j>=0;--j)
                {
                    const QString&ite=list[j];
                    if((ite.count("{")>=ite.count("}"))&&(ite.count("}")!=0))
                        st=st+1;
                }
            }
            codefile->close();
            return st;
        }
    }
}

int Obj_metrics::NORM() //кількість викликів віддалених методів
{
    if (codefile->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(codefile);
        while (!in.atEnd())
        {
            QString code = in.readAll();
            QStringList lst=code.split(QRegExp("class"),QString::SkipEmptyParts);
            int st=0;
            for(int i=lst.count()-1;i>=0;--i)
            {
                const QString&item=lst[i];
                QStringList list=item.split("){",QString::SkipEmptyParts);
                st += item.count("this->");
            }
            codefile->close();
            return  st;
        }
    }
}

float Obj_metrics::NOC() //кількість нащадків (безпосередніх)
{
    if (codefile->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(codefile);
        while (!in.atEnd())
        {
            QString code = in.readAll();
            QStringList lst=code.split(QRegExp("class"),QString::SkipEmptyParts);
                QStringList lis,Lclss,Rclss;
                for(int g=0;g<lst.count();g++)
                {
                    const QString&item=lst[g].trimmed();
                    QString St="";
                    for(int i=0;i<item.count();i++)
                    if (item[i]!='\n')
                        St=St+QString(item[i]);
                    else
                        break;
                    lis<<St;
                }
                for(int g=0;g<lis.count();g++)
                {
                    const QString&item=lis[g].trimmed();
                    QString St="";
                    for(int i=0;i<item.count();i++)
                    if(item[i]!=' ')
                        St=St+QString(item[i]);
                    else
                        break;
                    Rclss<<St;
                }
                for(int g=0;g<lis.count();g++)
                {
                    const QString&item=lis[g].trimmed();
                    QString St="";
                    lst=item.split(QRegExp("public|protected|private"),QString::SkipEmptyParts);
                    for(int i=0;i<lst.count();i++)
                    {
                        St=St+lst[i];
                    }
                    Lclss<<St;
                }
                for(int g=0;g<Rclss.count();g++)
                {
                    Lclss[g].replace(0,Rclss[g].count(),"");
                    Lclss[g]=Lclss[g].trimmed();
                }
                int st=0;
                for(int g=0;g<Rclss.count();g++)
                {
                    Rclss[g].remove(":");
                    for(int i=0;i<Rclss.count();i++)
                    {
                        st= st+Lclss[i].count(Rclss[g]);
                    }
                }
                codefile->close();
                return  st;
        }
    }
}


int Obj_metrics::RFC()  //	відгук на клас кількість методів, які можуть викликатися примірниками класу, обчислюється як сума локальних і віддалених методів;
{
    if (codefile->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(codefile);
        while (!in.atEnd())
        {
            QString code = in.readAll();
            QStringList lst=code.split(QRegExp("class"),QString::SkipEmptyParts);
            int st=0;
            for(int i=lst.count()-1;i>=0;--i)
            {
             const QString&item=lst[i];
             QStringList list=item.split("){",QString::SkipEmptyParts);
             st=st+item.count(")")-item.count(")");
            }
            codefile->close();
            return st;
        }
    }
}



